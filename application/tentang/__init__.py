from flask import Blueprint, render_template

tentang = Blueprint('tentang', __name__, template_folder='templates')

@tentang.route('/')
def root():
    return render_template('index.html')
