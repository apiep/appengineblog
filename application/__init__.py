from application.pastee import pastee
from flask import Flask, redirect
from application.util import dateformat, login_url, logout_url, has_login, is_admin
from application.admin import admin
from application.blog import blog
from application.tentang import tentang

app = Flask(__name__)
app.config['DEBUG'] = False

app.jinja_env.filters['dateformat'] = dateformat
app.jinja_env.globals['login_url'] = login_url
app.jinja_env.globals['logout_url'] = logout_url
app.jinja_env.globals['has_login'] = has_login
app.jinja_env.globals['is_admin'] = is_admin

app.register_blueprint(blog, url_prefix='/blog')
app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(tentang, url_prefix='/tentang')
app.register_blueprint(pastee, url_prefix='/pastee')

@app.route('/')
def root():
    return redirect('/blog')
