
from google.appengine.api import users


class User(object):
    def __init__(self):
        self.login_url = users.create_login_url('/')
        self.logout_url = users.create_logout_url('/')
        if users.get_current_user():
            self.nickname = users.get_current_user().nickname()
            self.has_login = True
            if users.is_current_user_admin():
                self.is_admin = True
            else:
                self.is_admin = False
        else:
            self.has_login = False

def get_context(*args, **kwargs):
    context = dict(
        user=User()
    )
    return context

def dateformat(value, format="%H:%M %d-%m-%Y"):
    return value.strftime(format)

def has_login():
    return users.get_current_user()

def is_admin():
    return users.is_current_user_admin()

def login_url(value):
    return users.create_login_url(value)

def logout_url(value):
    return users.create_logout_url(value)
