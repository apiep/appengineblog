from google.appengine.ext import db

class Post(db.Model):
    title = db.StringProperty()
    content = db.TextProperty()
    created = db.DateTimeProperty(auto_now_add=True)
    href = db.StringProperty()

    @classmethod
    def query_by_href(cls, href):
        return cls.gql("WHERE href = :1", href)

    @classmethod
    def query_all(cls):
        return cls.gql("ORDER BY created DESC")
