from flask import Blueprint, render_template, request, redirect
from application.blog.models import Post


blog = Blueprint('blog', __name__, template_folder="templates")

@blog.route('/')
def root():
    context = dict(
        posts = Post.query_all()
    )
    return render_template("blog_list.html", **context)

@blog.route('/<href>')
def href(href):
    context = dict(
        post=Post.query_by_href(href).get()
    )
    return render_template("blog_detail.html", **context)
