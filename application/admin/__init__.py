from application.blog import Post
from application.decorator import admin_required
from flask import Blueprint, render_template, request, redirect, url_for

admin = Blueprint('admin', __name__, template_folder='templates')

@admin.route('/')
@admin_required
def admin_root():
    context = dict(
        posts=Post.query_all()
    )
    return render_template('home.html', **context)

@admin.route('/addpost', methods=['POST'])
@admin_required
def addpost():
    if request.method == 'POST':
        post = Post()
        post.title = request.form['title']
        post.content = request.form['content']
        post.href = request.form['title'].strip().lower().replace(' ','-')
        post.put()
        return redirect('/admin')

@admin.route('/editpost', methods=['POST'])
@admin.route('/editpost/<href>', methods=['GET'])
@admin_required
def editpost(href=None):
    if request.method == 'POST':
        post = Post.query_by_href(request.form['href_old']).get()
        post.title = request.form['title']
        post.content = request.form['content']
        post.href = request.form['title'].strip().lower().replace(' ','-')
        Post.save(post)
        return redirect('/admin')
    else:
        post = Post.query_by_href(href).get()
        context = dict(
            post=post
        )
        return render_template('edit.html', **context)

@admin.route('/deletepost/<href>')
def deletepost(href=None):
    post = Post.query_by_href(href).get()
    post.delete()
    return redirect('/admin')