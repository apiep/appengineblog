from google.appengine.ext import db

class Pastee(db.Model):
    title = db.StringProperty()
    language = db.StringProperty()
    content = db.TextProperty()
    created = db.DateTimeProperty(auto_now_add=True)
    href = db.StringProperty()

    @classmethod
    def query(cls):
        return cls.gql("ORDER BY created DESC")

    @classmethod
    def query_by_href(cls, href):
        return cls.gql("WHERE href = :1", href).get()

    @classmethod
    def query_with_limit(cls, limit):
        return cls.gql('ORDER BY created DESC').fetch(limit=limit)