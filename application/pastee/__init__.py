from application.pastee.model import Pastee
from flask import Blueprint, render_template, request, redirect
from random import randrange

pastee = Blueprint('pastee', __name__, template_folder='templates')

@pastee.route('/')
def pastee_root():
    context = dict(
        pastee_list=Pastee.query_with_limit(10)
    )
    return render_template('pastee_home.html', **context)

@pastee.route('/<href>')
def pastee_hasil(href):
    context = dict(
        pastee=Pastee.query_by_href(href),
        pastee_list=Pastee.query_with_limit(10)
    )
    return render_template('pastee_hasil.html', **context)

@pastee.route('/addpastee', methods=['POST'])
def pastee_add():
    href = None
    if request.method == 'POST':
        p = Pastee()
        p.title = request.form['title']
        p.language = request.form['language']
        p.content = request.form['content']
        href = request.form['title'].strip().lower().replace(' ','-')
        href = href + str(randrange(0, 1000000, 10))
        p.href = href
        p.put()
    return redirect('/pastee/{0}'.format(href)) if href else redirect('/pastee')