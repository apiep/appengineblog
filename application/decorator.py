from functools import wraps
from google.appengine.api import users
from flask import redirect

def admin_required(func_name):
    @wraps(func_name)
    def decorated(*args, **kwargs):
        if users.get_current_user():
            if users.is_current_user_admin():
                return func_name(*args, **kwargs)
            else:
                return redirect('/')
        else:
            return redirect(users.create_login_url('/'))
    return decorated
